# Title
**How does browser render HTML, CSS and JS to DOM? What is the mechanism behind it**
# What is Browser Rendering?
Browser Rendering is a process where all elements of the web work together and create a UI. Whenever we are working on the website, there are certain things that are very essential for a good user experience. Some of the problems that may occur are slow loading of the website, waiting for an unnecessary file to download for initial render, a flash of unstyled content, etc.

To avoid such a problem, we need to understand how the browser rendering work and the lifecycle of the browser render a typical webpage. 

### There are three very essential things in the process of browser rendering.
1. DOM
2. CSSOM
3. RENDER TREE

## Document Object model(DOM)
When the browser reads HTML code and whenever it encounters HTML elements like HTML, body, head, li, etc. It creates a javascript object called **NODE**. Eventually, all HTML elements are converted into a Javascript object.

Since every HTML element has different properties, the NODE object will be created from different classes or constructor functions. For example NODE object of the div element is
HTMLdivElemnt, which has properties of the NODE class.

When the browser completes the creation of the NODE Object from the HTML document, it has to create a tree-like structure of these NODE objects, using the node objects browser replicate nesting of the HTML element and this will help the browser to efficiently render and manage the webpage throughout its lifecycle.

### HTML code --
```
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="">
    <title>Document</title>
</head>
<body>
    <h1></h1>
    <p></p>
    <div></div>   
    <script src=""></script>   
</body>
</html>
```

### DOM TREE 

![images](./DOM_Tree.png)

A DOM tree starts from our topmost element that is HTML and deep down as an occurrence or nesting of the element. DOM is a high-level web API provided by the browser to efficiently render and publically expose to the developer to manipulate the code of the webpage.

## CSS Object Model(CSSOM)
CSSOM is a set of APIs that allows manipulating CSS from javascript. It is much like DOM but for the CSS part. It allows users to read and modify CSS dynamically.

When we write **CSS** to design a webpage, CSS applies to every element by the CSSOM. Using CSS selectors we can target DOM elements and set a value of target element such as *height*, *width*, etc. When we apply CSS to HTML element through any methods like external, internal, Browser done all heavy lifting to apply CSS on DOM tree.

let's take an example and apply CSS to our above HTML code --
```
div{
height:"100px",
widht:"200px"
}
body{
    background-color:"white"
}
```
After constructing the dom, Browser reads CSS from all resources and attaches that CSS to an element, and this form is called CSSOM.

We can visualize CSSOM from the above DOM tree, In the above CSS, we are setting value on div and body element so CSSOM creates one tree-like structure as DOM and apply CSS.

## Render Tree 
Render Tree is a combination of DOM tree and CSSOM tree, it is also has a tree-like structure. The Browser has to calculate the layout of all visible elements and paint them on the screen for this browser uses Render Tree.

A Render Tree did not have nodes that have no area on pixel  (like display: none it doesn't have any area on the screen). It is a low-level representation of what will eventually be printed on the screen.

![images](./Render_Tree.png)
As you can see from the above image Render tree is a combination of DOM and CSSOM. Unlike DOM API which gives access to the DOM elements in the DOM tree constructed by the browser, CSSOM is kept hidden from the user. But since the browser combines DOM and CSSOM to form the Render Tree, the browser exposes the CSSOM node of a DOM element by providing high-level API on the DOM element itself. This enables the developer to access or change the CSS properties of a CSSOM node.

## Rendering Sequence
When the first webpage loaded browser read the HTML document construct the DOM and read the CSS and construct CSSOM and by combining DOM and CSSOM Browser create a Render tree and printed all visual on the screen.

In Rendering sequence there are multiple operations and processes --

### Layout Operation 
The first browser creates the layout of each individual Render-Tree node. The layout consists of the size of each node in pixels and where (position) it will be printed on the screen. This process is called layout since the browser is calculating the layout information of each node. This process is also called reflow or browser reflow and it can also occur when you scroll, resize the window or manipulate DOM elements.

### Paint Operation
After the layout operation we have a geometry that should be printed on the screen and elements (or a sub-tree) in the Render-Tree can overlap each other and they can have CSS properties that make them frequently change the look, position, or geometry (such as animations), the browser creates a layer for it. Creating a layer help browser efficiently paint the screen. Now that we have layers, we can combine them and draw them on the screen. But the browser does not draw all the layers in a single go. Each layer is drawn separately first.

In this operation, Browser converts the Render tree into on-screen pixels.

### Composition Operation
We have different layers or bitmap images that should be drawn on the screen in a specific order. In compositing operations, these layers are sent to GPU to finally draw it on the screen.

Sending entire layers to draw is inefficient because this has to happen every time there is a reflow *(layout)* or repaint. Hence, a layer is broken down into different tiles which then will be drawn on the screen. You can also visualize these tiles in Chrome’s DevTool Rendering panel.

### Browser Engine
Browser Engine is a piece of software that performs some operations like creating a DOM tree, CSSOM tree, and Render tree and also handling the rendering logic. This browser engine contains all the necessary elements and logic to render a web page from HTML code to actual pixels on the screen.

## Rendering Process in Browser
We know every browser has a layout engine or browser engine that can handle all necessary things in the process of rending a webpage, Like chrome have a V8 engine.

However, that’s not the case with how a browser renders things. HTML, CSS, or JavaScript, these languages are standardized by some entity or some organization. However, how a browser manages them together to render things on the screen is not standardized. The browser engine of Google Chrome might do things differently than the browser engine of Safari.

### Parsing and External resource
Parsing is the process of reading HTML elements and constructing a DOM tree and this process is also known as DOM parsing and a program that does that is called the DOM parser.

When the browser request a webpage and the server responds with some HTML text (with Content-Type header set to text/HTML), a browser may start parsing the HTML as soon as a few characters or lines of the entire document are available. Hence the browser can build the DOM tree incrementally, one node at a time. The browser parses HTML from top to bottom and not anywhere in the middle since the HTML represents a nested tree-like structure.

The most important thing to remember is the DOM parsing normally happens on the main thread. So if the main JavaScript execution thread is busy, DOM parsing will not progress until the thread is free. Why that’s so important you may ask? Because script elements are parser-blocking. Every external file request such as image, stylesheet, pdf, video, etc. does not block DOM construction (parsing) except script (.js) file requests.

### Parser Blocking script
A parser-blocking script is a script (JavaScript) file/code that stops the parsing of the HTML. When the browser encounters a script element, if it is an embedded script, then it will execute that script first and then continue parsing the HTML to construct the DOM tree. So all embedded scripts are parser-blocking, end of the discussion.

If the script element is an external script file, the browser will start the download of the external script file off the main thread but it will halt the execution of the main thread until that file is downloaded. That means no more DOM parsing until the script file is downloaded.

### Render-Blocking Css
As we learned, any external resource request except a parser-blocking script file doesn’t block the DOM parsing process. Hence CSS (including embedded) doesn’t block the DOM parser…(wait for it)…directly. Yes, CSS can block DOM parsing but for that, we need to understand the rendering processes.

The browser engines inside your browser construct the DOM tree from HTML content received as a text document from the server. Similarly, it constructs the CSSOM tree from the stylesheet content such as from an external CSS file or embedded (as well as inline) CSS in the HTML.

Both DOM and CSSOM tree constructions happen on the main thread and these trees are getting constructed concurrently. Together they form the Render Tree that is used to print things on the screen which is also getting built incrementally as the DOM tree is getting constructed.

**Reference**
1. https://medium.com/jspoint/how-the-browser-renders-a-web-page-dom-cssom-and-rendering-df10531c9969
2. https://developer.mozilla.org/en-US/docs/Web/Performance/How_browsers_work
